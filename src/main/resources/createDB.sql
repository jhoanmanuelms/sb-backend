﻿-- Created by Vertabelo (http://vertabelo.com)
-- Script type: create
-- Scope: [tables, references, sequences, views, procedures]
-- Generated at Sat Dec 06 17:42:58 UTC 2014

-- tables
-- Table: CLIENTE
CREATE TABLE CLIENTE (
    cedula int  NOT NULL,
    nombre varchar(100)  NOT NULL,
    direccion varchar(200)  NOT NULL,
    telefono varchar(20)  NOT NULL,
    CONSTRAINT CLIENTE_pk PRIMARY KEY (cedula)
);

-- Table: CUENTA
CREATE TABLE CUENTA (
    numero bigserial  NOT NULL,
    saldo decimal(50,2)  NOT NULL,
    estado_cuenta_id int  NOT NULL,
    cliente_cedula int  NOT NULL,
    CONSTRAINT CUENTA_pk PRIMARY KEY (numero)
);

-- Table: ESTADO_CUENTA
CREATE TABLE ESTADO_CUENTA (
    id int  NOT NULL,
    descripcion varchar(20)  NOT NULL,
    CONSTRAINT ESTADO_CUENTA_pk PRIMARY KEY (id)
);

-- Table: MOVIMIENTO
CREATE TABLE MOVIMIENTO (
    id bigserial  NOT NULL,
    fecha date  NOT NULL,
    valor decimal(50,2)  NOT NULL,
    tipo_movimiento_id int  NOT NULL,
    cuenta_numero int8  NOT NULL,
    CONSTRAINT MOVIMIENTO_pk PRIMARY KEY (id)
);

-- Table: ROL
CREATE TABLE ROL (
    id int  NOT NULL,
    descripcion varchar(20)  NOT NULL,
    CONSTRAINT ROL_pk PRIMARY KEY (id)
);

-- Table: TIPO_MOVIMIENTO
CREATE TABLE TIPO_MOVIMIENTO (
    id int  NOT NULL,
    descripcion varchar(20)  NOT NULL,
    CONSTRAINT TIPO_MOVIMIENTO_pk PRIMARY KEY (id)
);

-- Table: USUARIO
CREATE TABLE USUARIO (
    codigo int  NOT NULL,
    cedula int  NOT NULL,
    nombre varchar(100)  NOT NULL,
    password varchar(15)  NOT NULL,
    rol_id int  NOT NULL,
    CONSTRAINT USUARIO_pk PRIMARY KEY (codigo)
);

-- foreign keys
-- Reference:  CUENTA_CLIENTE (table: CUENTA)


ALTER TABLE CUENTA ADD CONSTRAINT CUENTA_CLIENTE 
    FOREIGN KEY (cliente_cedula)
    REFERENCES CLIENTE (cedula)
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  CUENTA_ESTADO_CUENTA (table: CUENTA)


ALTER TABLE CUENTA ADD CONSTRAINT CUENTA_ESTADO_CUENTA 
    FOREIGN KEY (estado_cuenta_id)
    REFERENCES ESTADO_CUENTA (id)
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  MOVIMIENTO_CUENTA (table: MOVIMIENTO)


ALTER TABLE MOVIMIENTO ADD CONSTRAINT MOVIMIENTO_CUENTA 
    FOREIGN KEY (cuenta_numero)
    REFERENCES CUENTA (numero)
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  MOVIMIENTO_TIPO_MOVIMIENTO (table: MOVIMIENTO)


ALTER TABLE MOVIMIENTO ADD CONSTRAINT MOVIMIENTO_TIPO_MOVIMIENTO 
    FOREIGN KEY (tipo_movimiento_id)
    REFERENCES TIPO_MOVIMIENTO (id)
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  USUARIO_ROL (table: USUARIO)

ALTER TABLE USUARIO ADD CONSTRAINT USUARIO_ROL 
    FOREIGN KEY (rol_id)
    REFERENCES ROL (id)
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Valores por defecto
INSERT INTO ESTADO_CUENTA VALUES(1, 'Activa');
INSERT INTO ESTADO_CUENTA VALUES(2, 'Inactiva');
INSERT INTO ESTADO_CUENTA VALUES(3, 'Cancelada');
INSERT INTO TIPO_MOVIMIENTO VALUES(1, 'Debito');
INSERT INTO TIPO_MOVIMIENTO VALUES(2, 'Credito');

-- End of file.

