package com.payu.bs.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.springframework.util.StringUtils;

import com.payu.bs.controller.ControladorCliente;
import com.payu.bs.controller.ControladorEstadoCuenta;
import com.payu.bs.entity.Cliente;
import com.payu.bs.entity.Cuenta;
import com.payu.bs.entity.EstadoCuenta;
import com.payu.bs.entity.Movimiento;
import com.payu.bs.pojo.POJOCliente;

/**
 * Clase que contiene la funcionalidad de acceso a la base de datos y funciona como capa de comunicacion.
 * 
 * @author Jhoan Mu�oz
 */
public class SBDAO
{
    /**
     * Almacena la entidad recibida en la base de datos.
     *
     * @param entidad Entidad a almacenar.
     */
    public static void guardar(Object entidad)
    {
        Session session = crearSesion();
        session.save(entidad);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Consulta a la base de datos el cliente identificado con la cedula entregada.
     *
     * @param cedula Cedula del cliente a consultar.
     *
     * @return Una instancia de la entidad {@link Cliente} que contiene la informacion.
     */
    public static Cliente consultarCliente(Integer cedula)
    {
        Session session = crearSesion();
        Cliente cliente = (Cliente)session.get(Cliente.class, cedula);

        return cliente;
    }

    /**
     * Consulta a la base de datos la cuenta correspondiente al numero entregado.
     *
     * @param numero Numero de la cuenta a consultar.
     *
     * @return Una instancia de la entidad {@link Cuenta} que contiene la informacion.
     */
    public static Cuenta consultarCuenta(Integer numero)
    {
        Session session = crearSesion();
        Cuenta cuenta = (Cuenta)session.get(Cuenta.class, numero);

        return cuenta;
    }

    /**
     * Consulta todos los estados de cuenta almacenados en la base de datos.
     *
     * @return Una lista de objetos {@link EstadoCuenta}.
     */
    @SuppressWarnings("unchecked")
    public static List<EstadoCuenta> consultarEstados()
    {
        Session session = crearSesion();
        List<EstadoCuenta> estados = session.createCriteria(EstadoCuenta.class).list();
        session.close();

        return estados;
    }

    /**
     * Consulta todos los clientes almacenados en la base de datos.
     *
     * @return Una lista de objetos {@link Cliente}.
     */
    @SuppressWarnings("unchecked")
    public static List<Cliente> consultarClientes()
    {
        Session session = crearSesion();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        session.close();

        return clientes;
    }

    /**
     * Actualiza la informacion del cliente identificado con el numero de cedula almacenado
     * en el objeto POJO recibido.
     *
     * @param pojoCliente Instancia de {@link POJOCliente} con la informacion a actualizar.
     */
    public static void actualizarCliente(POJOCliente pojoCliente)
    {
        Session session = crearSesion();
        Cliente cliente = (Cliente)session.get(Cliente.class, pojoCliente.getCedula());

        String nombre =
            StringUtils.isEmpty(pojoCliente.getNombre()) ?
                cliente.getNombre() : pojoCliente.getNombre();

        String direccion =
            StringUtils.isEmpty(pojoCliente.getDireccion()) ?
                cliente.getDireccion() : pojoCliente.getDireccion();

        String telefono =
            StringUtils.isEmpty(pojoCliente.getTelefono()) ?
                cliente.getTelefono() : pojoCliente.getTelefono();

        cliente.setNombre(nombre);
        cliente.setDireccion(direccion);
        cliente.setTelefono(telefono);

        session.save(cliente);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Elimina el cliente identificado con la cedula especificada. Las cuentas asociadas al cliente eliminado
     * seran inactivadas.
     *
     * @param cedula Numero de cedula del cliente a eliminar.
     */
    public static void eliminarCliente(Integer cedula)
    {
        Session session = crearSesion();
        Cliente cliente = (Cliente)session.get(Cliente.class, cedula);
        Cliente clienteInactivo =
            (Cliente)session.get(Cliente.class, ControladorCliente.CEDULA_CLIENTE_CUENTAS_INACTIVAS);

        for (Cuenta cuenta : cliente.getCuentas())
        {
            cuenta.setCliente(clienteInactivo);
            cuenta.setEstado(ControladorEstadoCuenta.CUENTA_INACTIVA_ID);
            session.save(cuenta);
        }

        session.delete(cliente);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Elimina la cuenta con el numero especificado.
     *
     * @param numero Numero de la cuenta a eliminar.
     */
    public static void eliminarCuenta(Integer numero)
    {
        Session session = crearSesion();
        Cuenta cuenta = (Cuenta)session.get(Cuenta.class, numero);
        cuenta.setEstado(ControladorEstadoCuenta.CUENTA_CANCELADA_ID);

        session.save(cuenta);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Actualiza el saldo de la cuenta identificada con el numero entregado a tener el nuevo valor.
     *
     * @param nroCuenta Numero de la cuenta cuyo saldo sera actualizado.
     * @param nuevoSaldo Nuevo saldo a asignar a la cuenta.
     */
    public static void actualizarSaldo(Integer nroCuenta, Double nuevoSaldo)
    {
        Session session = crearSesion();
        Cuenta cuenta = (Cuenta)session.get(Cuenta.class, nroCuenta);

        cuenta.setSaldo(nuevoSaldo);
        session.save(cuenta);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Consulta a la base de datos los movimientos correspondientes a la cuenta especificada dentro de
     * las fechas entregadas.
     *
     * @param nroCuenta Numero de la cuenta utilizada para generar el reporte.
     * @param fechaInicial Fecha inicial desde la que se consultaran los movimientos.
     * @param fechaFinal Fecha final hasta donde se consultaran los movimientos.
     *
     * @return Lista de {@link Movimiento} que cumplen con los criterios especificados.
     */
    @SuppressWarnings("unchecked")
    public static List<Movimiento> reporteMovimientos(Integer nroCuenta, Date fechaInicial, Date fechaFinal)
    {
        Session session = crearSesion();
        Cuenta cuenta = (Cuenta)session.get(Cuenta.class, nroCuenta);
        Criteria criteria = session.createCriteria(Movimiento.class);
        criteria.addOrder(Order.asc(Movimiento.NOMBRE_COLUMNA_FECHA));
        criteria.add(Restrictions.eq(Movimiento.NOMBRE_PROPIEDAD_CUENTA, cuenta));
        criteria.add(Restrictions.between(Movimiento.NOMBRE_COLUMNA_FECHA, fechaInicial, fechaFinal));

        return criteria.list();
    }

    /**
     * Crea un objeto {@link Session} que puede ser utilizado para realizar consultas a la base de datos. Dicha sesion
     * esta conectada y con transaccion iniciada al momento de ser devuelta asi que no es necesario realizar estos
     * procesos cada vez.
     *
     * @return Un objeto {@link Session} que puede ser utilizado para realizar consultas a la base de datos.
     */
    private static Session crearSesion()
    {
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry  serviceRegistry =
            new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        return session;
    }
}
