package com.payu.bs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration 
@Configuration 
@ComponentScan
/**
 * Clase principal que ejecuta la funcionalidad necesaria para hacer accesible la aplicacion.
 *
 * @author Jhoan Mu�oz
 */
public class Application
{
    public static void main(String args[])
    {
        SpringApplication.run(Application.class, args);
    }
}