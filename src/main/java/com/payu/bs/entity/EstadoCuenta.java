package com.payu.bs.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Clase utilizada para representar la entidad estado de cuenta.
 *
 * @author Jhoan Mu�oz
 */
@Entity
@Table(name = "estado_cuenta")
public class EstadoCuenta
{
    @Id
    private Integer id;
    private String descripcion;

    public Integer getId()
    {
        return id;
    }
    public void setId(Integer id)
    {
        this.id = id;
    }
    public String getDescripcion()
    {
        return descripcion;
    }
    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }
}
