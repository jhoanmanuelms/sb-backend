package com.payu.bs.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase utilizada para representar la entidad movimiento.
 *
 * @author Jhoan Mu�oz
 */
@Entity
@Table(name = "movimiento")
public class Movimiento
{
    /** Nombre de la columna de la base de datos que almacena la fecha del movimiento. */
    public static final String NOMBRE_COLUMNA_FECHA = "fecha";

    /** Nombre de la propiedad que almacena la cuenta a la que esta asociada el movimiento. */
    public static final String NOMBRE_PROPIEDAD_CUENTA = "cuenta";

    /** Identificador de la columna que almacena el tipo de movimiento. */
    private static final String ID_COLUMNA_TIPO_MOVIMIENTO = "tipo_movimiento_id";

    /** Identificador para la secuencia que genera el numero de movimiento. */
    private static final String ID_SECUENCIA_NUMERO_MVTO = "secuencia_numero_mvto";

    /** Nombre de la secuencia que genera el numero de movimiento. */
    private static final String NOMBRE_SECUENCIA_NUMERO_MVTO = "movimiento_id_seq";

    @Id
    @SequenceGenerator(name=ID_SECUENCIA_NUMERO_MVTO, sequenceName=NOMBRE_SECUENCIA_NUMERO_MVTO)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator=ID_SECUENCIA_NUMERO_MVTO)
    private Integer id;
    private Date fecha;
    private Double valor;

    @Column(name = ID_COLUMNA_TIPO_MOVIMIENTO)
    private Integer tipoMovimiento;

    @ManyToOne(fetch = FetchType.LAZY)
    private Cuenta cuenta;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Double getValor()
    {
        return valor;
    }

    public void setValor(Double valor)
    {
        this.valor = valor;
    }

    public Integer getTipoMovimiento()
    {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(Integer tipoMovimiento)
    {
        this.tipoMovimiento = tipoMovimiento;
    }

    public Cuenta getCuenta()
    {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta)
    {
        this.cuenta = cuenta;
    }
}
