package com.payu.bs.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Clase utilizada para representar la entidad cliente.
 *
 * @author Jhoan Mu�oz
 */
@Entity
@Table(name="cliente")
public class Cliente
{
    @Id
    private Integer cedula;
    private String nombre;
    private String direccion;
    private String telefono;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    private List<Cuenta> cuentas;

    public int getCedula()
    {
        return cedula;
    }

    public void setCedula(int cedula)
    {
        this.cedula = cedula;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDireccion()
    {
        return direccion;
    }

    public void setDireccion(String direccion)
    {
        this.direccion = direccion;
    }

    public String getTelefono()
    {
        return telefono;
    }

    public void setTelefono(String telefono)
    {
        this.telefono = telefono;
    }

    public List<Cuenta> getCuentas()
    {
        return cuentas;
    }

    public void setCuentas(List<Cuenta> cuentas)
    {
        this.cuentas = cuentas;
    }
}
