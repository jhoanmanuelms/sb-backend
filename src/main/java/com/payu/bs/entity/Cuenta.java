package com.payu.bs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase utilizada para representar la entidad cuenta.
 *
 * @author Jhoan Mu�oz
 */
@Entity
@Table(name="cuenta")
public class Cuenta
{
    @Id
    @SequenceGenerator(name="secuencia_numero_cta", sequenceName="cuenta_numero_seq")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="secuencia_numero_cta")
    private Integer numero;

    @Column(name="estado_cuenta_id")
    private Integer estado;
    private Double saldo;

    @ManyToOne(fetch=FetchType.LAZY)
    private Cliente cliente;

    public Integer getNumero()
    {
        return numero;
    }
    public void setNumero(Integer numero)
    {
        this.numero = numero;
    }
    public Integer getEstado()
    {
        return estado;
    }
    public void setEstado(Integer estado)
    {
        this.estado = estado;
    }
    public Double getSaldo()
    {
        return saldo;
    }
    public void setSaldo(Double saldo)
    {
        this.saldo = saldo;
    }
    public Cliente getCliente()
    {
        return cliente;
    }
    public void setCliente(Cliente cliente)
    {
        this.cliente = cliente;
    }
}
