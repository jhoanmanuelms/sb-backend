package com.payu.bs.controller;

import java.util.HashMap;
import java.util.Map;

import com.payu.bs.dao.SBDAO;
import com.payu.bs.entity.EstadoCuenta;

/**
 * Controlador para la funcionalidad relacionada con el estado de cuentas.
 *
 * @author Jhoan Mu�oz
 */
public class ControladorEstadoCuenta
{
    /** Identificador utilizado para el estado de cuenta activada. */
    public static final Integer CUENTA_ACTIVADA_ID = 1;

    /** Identificador utilizado para el estado de cuenta inactiva. */
    public static final Integer CUENTA_INACTIVA_ID = 2;

    /** Identificador utilizado para el estado de cuenta inactiva. */
    public static final Integer CUENTA_CANCELADA_ID = 3;

    /** Cache para almacenar los estados de cuenta registrados en la base de datos. */
    private static Map<Integer, String> cacheEstados = null;

    /**
     * Devuelve la cache de estados de la aplicacion. La estados solo seran consultados a la base de datos
     * una vez.
     *
     * @return Un mapa con todos los estados de la aplicacion utilizando el id como llave para obtener la
     *         descripcion.
     */
    public static Map<Integer, String> getCacheEstados()
    {
        if (cacheEstados == null)
        {
            cacheEstados = new HashMap<Integer, String>();
            for (EstadoCuenta estado : SBDAO.consultarEstados())
            {
                cacheEstados.put(estado.getId(), estado.getDescripcion());
            }
        }

        return cacheEstados;
    }
}
