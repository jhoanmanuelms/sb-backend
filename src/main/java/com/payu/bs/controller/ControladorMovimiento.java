package com.payu.bs.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.payu.bs.dao.SBDAO;
import com.payu.bs.entity.Cuenta;
import com.payu.bs.entity.Movimiento;
import com.payu.bs.pojo.POJOMovimiento;
import com.payu.bs.pojo.POJOReporteMovimientos;

/**
 * Controlador para la funcionalidad relacionada con el modulo de movimientos.
 *
 * @author Jhoan Mu�oz
 */
@RestController
public class ControladorMovimiento
{
    /** Patron usado para formatear las fechas. */
    private static final String FORMATO_FECHA = "yyyy-MM-dd";

    /** Identificador utilizado para los movimientos de tipo debito. */
    private static final int ID_MOVIMIENTO_DEBITO = 1;

    /** Identificador utilizado para los movimientos de tipo credito. */
    private static final int ID_MOVIMIENTO_CREDITO = 2;

    /** Nombre utilizado para los movimientos de tipo debito. */
    private static final String NOMBRE_MOVIMIENTO_DEBITO = "Debito";

    /** Nombre utilizado para los movimientos de tipo credito. */
    private static final String NOMBRE_MOVIMIENTO_CREDITO = "Credito";

    /** Minimo saldo permitido para una cuenta despues de realizar una transaccion. */
    private static final Double SALDO_MINIMO_PERMITIDO = 1D;

    /** URL utilizada como identificador para el servicio REST de registro de movimientos. */
    private static final String URL_REGISTRO_MOVIMIENTO = "/registrarMovimiento";

    /** URL utilizada como identificador para el servicio REST de reporte de movimientos. */
    private static final String URL_REPORTE_MOVIMIENTOS = "/reporteMovimientos";

    /**
     * Registra un movimiento o transaccion bancaria utilizando la informacion contenida en el
     * objeto JSON entregado.
     *
     * @param movimientoJSON Objeto JSON que contiene la informacion necesaria para registrar la
     *        transaccion. Se espera que el objeto contenga el numero de la cuenta y el valor
     *        y el tipo del movimiento.
     *
     * @return El nuevo saldo de la cuenta despues del movimiento.
     *
     * @throws IllegalArgumentException En caso que la transaccion no cumpla con la restriccion de
     *         dejar el saldo de la cuenta por lo menos con el valor minimo permitido.
     */
    @RequestMapping(value = URL_REGISTRO_MOVIMIENTO, method = RequestMethod.POST)
    public Double registrarMovimiento(@RequestBody POJOMovimiento movimientoJSON)
    throws IllegalArgumentException
    {
        Movimiento movimiento = new Movimiento();
        Cuenta cuenta = SBDAO.consultarCuenta(movimientoJSON.getNumeroCuenta());

        switch(movimientoJSON.getIdTipoMovimiento())
        {
            case ID_MOVIMIENTO_DEBITO:
                cuenta.setSaldo(cuenta.getSaldo() + movimientoJSON.getValor());
                break;

            case ID_MOVIMIENTO_CREDITO:
                if ((cuenta.getSaldo() - movimientoJSON.getValor()) >= SALDO_MINIMO_PERMITIDO)
                {
                    cuenta.setSaldo(cuenta.getSaldo() - movimientoJSON.getValor());
                }
                else
                {
                    throw new IllegalArgumentException(
                        "La cuenta no posee saldo suficiente para realizar la transaccion");
                }
                break;
        }

        movimiento.setCuenta(cuenta);
        movimiento.setFecha(new Date());
        movimiento.setValor(movimientoJSON.getValor());
        movimiento.setTipoMovimiento(movimientoJSON.getIdTipoMovimiento());

        SBDAO.guardar(movimiento);
        SBDAO.actualizarSaldo(movimientoJSON.getNumeroCuenta(), cuenta.getSaldo());

        return cuenta.getSaldo();
    }

    /**
     * Consulta los movimientos registrados para la cuenta con el numero almacenado en el objeto
     * JSON dentro de las fechas especificadas en el mismo objeto.
     *
     * @param reporteJSON Objeto JSON que contiene la informacion necesaria para generar el
     *        reporte. Se espera que el objeto contenga el numero de la cuenta y las fechas
     *        para filtrar los datos.
     *
     * @return Una lista de {@link POJOMovimiento} con la informacion de los movimientos que
     *         cumplen con los criterios de busqueda.
     *
     * @throws ParseException Si hay algun error convirtiendo o formateando las fechas del reporte.
     */
    @RequestMapping(value = URL_REPORTE_MOVIMIENTOS, method = RequestMethod.POST)
    public List<POJOMovimiento> reporteMovimientos(@RequestBody POJOReporteMovimientos reporteJSON)
    throws ParseException
    {
        SimpleDateFormat format = new SimpleDateFormat(FORMATO_FECHA);
        Date fechaInicial = format.parse(reporteJSON.getFechaInicial());
        Date fechaFinal = format.parse(reporteJSON.getFechaFinal());

        List<POJOMovimiento> movimientos = new ArrayList<POJOMovimiento>();
        for (Movimiento movimiento :
            SBDAO.reporteMovimientos(reporteJSON.getNumeroCuenta(), fechaInicial, fechaFinal))
        {
            POJOMovimiento pojoMovimiento = new POJOMovimiento();
            pojoMovimiento.setIdTipoMovimiento(movimiento.getTipoMovimiento());
            pojoMovimiento.setValor(movimiento.getValor());
            pojoMovimiento.setFecha(format.format(movimiento.getFecha()));

            switch(movimiento.getTipoMovimiento())
            {
                case ID_MOVIMIENTO_DEBITO:
                    pojoMovimiento.setTipoMovimiento(NOMBRE_MOVIMIENTO_DEBITO);
                    break;
                case ID_MOVIMIENTO_CREDITO:
                    pojoMovimiento.setTipoMovimiento(NOMBRE_MOVIMIENTO_CREDITO);
                    break;
            }
            movimientos.add(pojoMovimiento);
        }

        return movimientos;
    }
}
