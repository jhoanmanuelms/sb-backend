package com.payu.bs.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.payu.bs.dao.SBDAO;
import com.payu.bs.entity.Cliente;
import com.payu.bs.pojo.POJOCliente;
import com.payu.bs.pojo.POJOCuenta;

/**
 * Controlador para la funcionalidad relacionada con el modulo de clientes.
 *
 * @author Jhoan Mu�oz
 */
@RestController
public class ControladorCliente
{
    /** Cedula utilizada para asignar las cuentas inactivas de los clientes que se eliminan. */
    public static final Integer CEDULA_CLIENTE_CUENTAS_INACTIVAS = -1;

    /** URL utilizada como identificador para el servicio REST de consulta de clientes. */
    private static final String URL_CONSULTA_CLIENTE = "/consultarCliente";

    /** URL utilizada como identificador para el servicio REST de consulta de todos los clientes. */
    private static final String URL_CONSULTA_TODOS_CLIENTES = "/consultarClientes";

    /** URL utilizada como identificador para el servicio REST de registro de clientes. */
    private static final String URL_REGISTRO_CLIENTE = "/registroCliente";

    /** URL utilizada como identificador para el servicio REST de edicion de clientes. */
    private static final String URL_EDICION_CLIENTE = "/editarCliente";

    /** URL utilizada como identificador para el servicio REST de eliminacion de clientes. */
    private static final String URL_ELIMINACION_CLIENTE = "/eliminarCliente";

    /**
     * Registra un nuevo cliente en la base de datos utilizando la informacion almacenada en el
     * objeto recibido.
     *
     * @param clienteJSON Objeto JSON que contiene la informacion del cliente a registrar.
     */
    @RequestMapping(value = URL_REGISTRO_CLIENTE, method = RequestMethod.POST)
    public void registrarCliente(@RequestBody POJOCliente clienteJSON)
    {
        Cliente cliente = new Cliente();
        cliente.setCedula(clienteJSON.getCedula());
        cliente.setDireccion(clienteJSON.getDireccion());
        cliente.setNombre(clienteJSON.getNombre());
        cliente.setTelefono(clienteJSON.getTelefono());

        SBDAO.guardar(cliente);
    }

    /**
     * Consulta el cliente identificado con la cedula almacenada en el objeto JSON recibido.
     *
     * @param clienteJSON Objeto JSON que contiene la informacion del cliente a consultar.
     *
     * @return Un objeto {@link POJOCliente} que contiene la informacion correspondiente al
     *         cliente consultado, incluyendo sus cuentas asociadas.
     */
    @RequestMapping(value = URL_CONSULTA_CLIENTE, method = RequestMethod.POST)
    public POJOCliente consultarCliente(@RequestBody POJOCliente clienteJSON)
    {
        POJOCliente pojoCliente = new POJOCliente();
        Cliente cliente = SBDAO.consultarCliente(clienteJSON.getCedula());
        Hibernate.initialize(cliente.getCuentas());

        pojoCliente.setCedula(cliente.getCedula());
        pojoCliente.setNombre(cliente.getNombre());
        pojoCliente.setDireccion(cliente.getDireccion());
        pojoCliente.setTelefono(cliente.getTelefono());
        pojoCliente.setCuentas(new ArrayList<POJOCuenta>());
        pojoCliente.agregarCuentas(cliente.getCuentas());

        return pojoCliente;
    }

    /**
     * Consulta todos los clientes almacenados en la base de datos.
     *
     * @return Una lista de todos los clientes almacenados en la base de datos representados como objetos
     *         {@link POJOCliente}
     */
    @RequestMapping(value = URL_CONSULTA_TODOS_CLIENTES, method = RequestMethod.POST)
    public List<POJOCliente> consultarClientes()
    {
        List<POJOCliente> clientes = new ArrayList<POJOCliente>();
        for(Cliente cliente : SBDAO.consultarClientes())
        {
            POJOCliente pojoCliente = new POJOCliente();
            pojoCliente.setCedula(cliente.getCedula());
            pojoCliente.setNombre(cliente.getNombre());
            pojoCliente.setDireccion(cliente.getDireccion());
            pojoCliente.setTelefono(cliente.getTelefono());
            clientes.add(pojoCliente);
        }

        return clientes;
    }

    /**
     * Edita la informacion del cliente identificado con el numero de cedula almacenado en el
     * objeto recibido. La informacion del objeto JSON sera utilizada para actualizar los datos
     * y solo seran tenidos en cuenta si no estan vacios.
     *
     * @param clienteJSON Objeto JSON que contiene la informacion del cliente a actualizar.
     */
    @RequestMapping(value = URL_EDICION_CLIENTE, method = RequestMethod.POST)
    public void editarCliente(@RequestBody POJOCliente clienteJSON)
    {
        SBDAO.actualizarCliente(clienteJSON);
    }

    /**
     * Elimina el cliente identificado con la cedula almacenada en el objeto JSON entregado. Las cuentas
     * asociadas al cliente eliminado seran inactivadas.
     *
     * @param clienteJSON Objeto JSON que contiene la informacion del cliente a eliminar.
     */
    @RequestMapping(value = URL_ELIMINACION_CLIENTE, method = RequestMethod.POST)
    public void eliminarCliente(@RequestBody POJOCliente clienteJSON)
    {
        SBDAO.eliminarCliente(clienteJSON.getCedula());
    }
}
