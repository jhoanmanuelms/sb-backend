package com.payu.bs.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.payu.bs.dao.SBDAO;
import com.payu.bs.entity.Cliente;
import com.payu.bs.entity.Cuenta;
import com.payu.bs.pojo.POJOCuenta;

/**
 * Controlador para la funcionalidad relacionada con el modulo de cuentas.
 *
 * @author Jhoan Mu�oz
 */
@RestController
public class ControladorCuenta
{
    /** URL utilizada como identificador para el servicio REST de registro de cuentas. */
    private static final String URL_REGISTRO_CUENTA = "/registroCuenta";

    /** URL utilizada como identificador para el servicio REST de consulta de cuentas. */
    private static final String URL_CONSULTA_CUENTA = "/consultarCuenta";

    /** URL utilizada como identificador para el servicio REST de eliminacion de cuentas. */
    private static final String URL_ELIMINACION_CUENTA = "/eliminarCuenta";

    /**
     * Registra una nueva cuenta en la base de datos utilizando la informacion almacenada en el
     * objeto JSON recibido. Las cuentas siempre se registraran con estado "Activada" por defecto.
     *
     * @param cuentaJSON Objeto JSON que contiene la informacion de la cuenta a registrar.
     */
    @RequestMapping(value = URL_REGISTRO_CUENTA, method = RequestMethod.POST)
    public void registrarCuenta(@RequestBody POJOCuenta cuentaJSON)
    {
        Cuenta cuenta = new Cuenta();
        Cliente cliente = SBDAO.consultarCliente(cuentaJSON.getCedulaCliente());

        cuenta.setCliente(cliente);
        cuenta.setEstado(ControladorEstadoCuenta.CUENTA_ACTIVADA_ID);
        cuenta.setSaldo(cuentaJSON.getSaldo());
        SBDAO.guardar(cuenta);
    }

    /**
     * Consulta la cuenta correspondiente al numero almacenado en el objeto JSON recibido.
     *
     * @param cuentaJSON Objeto JSON que contiene la informacion de la cuenta a consultar.
     *
     * @return Un objeto {@link POJOCuenta} que contiene la informacion correspondiente a la
     *         cuenta consultada, incluyendo la informacion del cliente.
     */
    @RequestMapping(value = URL_CONSULTA_CUENTA, method = RequestMethod.POST)
    public POJOCuenta consultarCuenta(@RequestBody POJOCuenta cuentaJSON)
    {
        POJOCuenta pojoCuenta = new POJOCuenta();
        Cuenta cuenta = SBDAO.consultarCuenta(cuentaJSON.getNumero());

        pojoCuenta.setSaldo(cuenta.getSaldo());
        pojoCuenta.setNumero(cuenta.getNumero());
        pojoCuenta.setCedulaCliente(cuenta.getCliente().getCedula());
        pojoCuenta.setNombreCliente(cuenta.getCliente().getNombre());
        pojoCuenta.setEstado(ControladorEstadoCuenta.getCacheEstados().get(cuenta.getEstado()));

        return pojoCuenta;
    }

    /**
     * Elimina la cuenta con el numero almacenado en el objeto JSON entregado.
     *
     * @param cuentaJSON Objeto JSON que contiene la informacion de la cuenta a eliminar.
     */
    @RequestMapping(value = URL_ELIMINACION_CUENTA, method = RequestMethod.POST)
    public void eliminarCuenta(@RequestBody POJOCuenta cuentaJSON)
    {
        SBDAO.eliminarCuenta(cuentaJSON.getNumero());
    }
}
