package com.payu.bs.pojo;

import java.util.List;

import com.payu.bs.controller.ControladorEstadoCuenta;
import com.payu.bs.entity.Cuenta;

/**
 * Clase utilizada para contener la informacion de un cliente y transportar esta informacion del backend al
 * frontend y viceversa.
 *
 * @author Jhoan Mu�oz
 */
public class POJOCliente
{
    private int cedula;
    private String nombre;
    private String direccion;
    private String telefono;
    private List<POJOCuenta> cuentas;

    public int getCedula()
    {
        return cedula;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getDireccion()
    {
        return direccion;
    }

    public String getTelefono()
    {
        return telefono;
    }

    public void setCedula(int cedula)
    {
        this.cedula = cedula;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion)
    {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono)
    {
        this.telefono = telefono;
    }

    public List<POJOCuenta> getCuentas()
    {
        return cuentas;
    }

    public void setCuentas(List<POJOCuenta> cuentas)
    {
        this.cuentas = cuentas;
    }

    public void agregarCuentas(List<Cuenta> cuentas)
    {
        for (Cuenta cuenta : cuentas)
        {
            POJOCuenta pojoCuenta = new POJOCuenta();
            pojoCuenta.setSaldo(cuenta.getSaldo());
            pojoCuenta.setNumero(cuenta.getNumero());
            pojoCuenta.setEstado(ControladorEstadoCuenta.getCacheEstados().get(cuenta.getEstado()));
            this.cuentas.add(pojoCuenta);
        }
    }
}
