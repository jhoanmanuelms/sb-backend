package com.payu.bs.pojo;


/**
 * Clase utilizada para contener la informacion de un movimiento y transportar esta informacion del backend al
 * frontend y viceversa.
 *
 * @author Jhoan Mu�oz
 */
public class POJOMovimiento
{
    private Integer numeroCuenta;
    private Integer idTipoMovimiento;
    private String tipoMovimiento;
    private Double valor;
    private String fecha;

    public Integer getNumeroCuenta()
    {
        return numeroCuenta;
    }
    public void setNumeroCuenta(Integer numeroCuenta)
    {
        this.numeroCuenta = numeroCuenta;
    }
    public Integer getIdTipoMovimiento()
    {
        return idTipoMovimiento;
    }
    public void setIdTipoMovimiento(Integer idTipoMovimiento)
    {
        this.idTipoMovimiento = idTipoMovimiento;
    }
    public Double getValor()
    {
        return valor;
    }
    public void setValor(Double valor)
    {
        this.valor = valor;
    }
    public String getFecha()
    {
        return fecha;
    }
    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }
    public String getTipoMovimiento()
    {
        return tipoMovimiento;
    }
    public void setTipoMovimiento(String tipoMovimiento)
    {
        this.tipoMovimiento = tipoMovimiento;
    }
}
