package com.payu.bs.pojo;

/**
 * Clase utilizada para contener la informacion utilizada para generar el reporte de movimientos y transportar esta informacion
 * del backend al frontend y viceversa.
 *
 * @author Jhoan Mu�oz
 */
public class POJOReporteMovimientos
{
    private Integer numeroCuenta;
    private String fechaInicial;
    private String fechaFinal;

    public Integer getNumeroCuenta()
    {
        return numeroCuenta;
    }
    public String getFechaInicial()
    {
        return fechaInicial;
    }
    public String getFechaFinal()
    {
        return fechaFinal;
    }
}
