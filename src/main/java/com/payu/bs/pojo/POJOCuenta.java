package com.payu.bs.pojo;

/**
 * Clase utilizada para contener la informacion de una cuenta y transportar esta informacion del backend al
 * frontend y viceversa.
 *
 * @author Jhoan Mu�oz
 */
public class POJOCuenta
{
    private Integer numero;
    private String estado;
    private Double saldo;
    private Integer cedulaCliente;
    private String nombreCliente;

    public Integer getNumero()
    {
        return numero;
    }
    public void setNumero(Integer numero)
    {
        this.numero = numero;
    }
    public String getEstado()
    {
        return estado;
    }
    public void setEstado(String estado)
    {
        this.estado = estado;
    }
    public Double getSaldo()
    {
        return saldo;
    }
    public void setSaldo(Double saldo)
    {
        this.saldo = saldo;
    }
    public Integer getCedulaCliente()
    {
        return cedulaCliente;
    }
    public void setCedulaCliente(Integer cedulaCliente)
    {
        this.cedulaCliente = cedulaCliente;
    }
    public String getNombreCliente()
    {
        return nombreCliente;
    }
    public void setNombreCliente(String nombreCliente)
    {
        this.nombreCliente = nombreCliente;
    }
}
